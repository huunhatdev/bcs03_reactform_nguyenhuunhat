export const validate = (data) => {
  let onFalse = false;
  let valid = {
    id: "",
    name: "",
    phone: "",
    email: "",
  };
  if (!/\d{6}/.test(data.id)) {
    valid.id = "Mã sinh viên bao gồm 6 số";
    onFalse = true;
  }
  if (
    !/^([A-VXYỲỌÁẦẢẤỜỄÀẠẰỆẾÝỘẬỐŨỨĨÕÚỮỊỖÌỀỂẨỚẶÒÙỒỢÃỤỦÍỸẮẪỰỈỎỪỶỞÓÉỬỴẲẸÈẼỔẴẺỠƠÔƯĂÊÂĐa-vxyỳọáầảấờễàạằệếýộậốũứĩõúữịỗìềểẩớặòùồợãụủíỹắẫựỉỏừỷởóéửỵẳẹèẽổẵẻỡơôưăêâđ]+)((\s{1}[A-VXYỲỌÁẦẢẤỜỄÀẠẰỆẾÝỘẬỐŨỨĨÕÚỮỊỖÌỀỂẨỚẶÒÙỒỢÃỤỦÍỸẮẪỰỈỎỪỶỞÓÉỬỴẲẸÈẼỔẴẺỠƠÔƯĂÊÂĐa-vxyỳọáầảấờễàạằệếýộậốũứĩõúữịỗìềểẩớặòùồợãụủíỹắẫựỉỏừỷởóéửỵẳẹèẽổẵẻỡơôưăêâđ]+){1,})$/.test(
      data.name
    )
  ) {
    valid.name = "Họ và tên không đúng định dạng";
    onFalse = true;
  }
  if (
    !/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/.test(data.phone) ||
    data.phone.length < 10
  ) {
    valid.phone = "Số điện thoại không đúng định dạng";
    onFalse = true;
  }
  if (!/^[\w]+([\.-]?[\w]+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$$/.test(data.email)) {
    valid.email = "Email không đúng định dạng";
    onFalse = true;
  }
  console.log(valid);
  return { valid, onFalse };
};
