import { AutoComplete, Input, Select } from "antd";
import React, { Component, PureComponent } from "react";
import { connect } from "react-redux";
import {
  DELETE_STUDENT,
  EDIT_STUDENT,
  ON_CHANGE_KEYWORD_SEARCH,
  ON_CHANGE_KEY_SEARCH,
} from "./redux/constant/constant";
const { Option } = Select;
class TableManage extends Component {
  componentDidUpdate(props) {}
  handleChangeSelect = (e) => {
    if (e.target.value === -1) {
      this.setState({
        keysearch: "",
        dataTable: this.props.dataStudent,
      });
    } else {
      this.setState({
        keysearch: e.target.value,
      });
    }
  };
  handleSearch = (e) => {
    let searchValue = e.target.value;
    let keySearch = this.state.keysearch;
    if (keySearch) {
      let searchArr = this.props.dataStudent.filter((e) => {
        return e[keySearch].includes(searchValue);
      });

      this.setState({
        dataTable: searchArr,
      });
    }
  };
  render() {
    return (
      <div>
        {/* <div className="input-group mb-3">
          <div className="input-group-prepend">
            <select
              className="custom-select"
              id="inputGroupSelect02"
              onChange={(e) => this.handleChangeSelect(e)}
            >
              <option value={-1} defaultValue>
                Choose...
              </option>
              <option value={"id"}>Mã SV</option>
              <option value={"name"}>Tên</option>
              <option value={"phone"}>Số điện thoại</option>
              <option value={"email"}>Email</option>
            </select>
          </div>
          <input
            type="text"
            className="form-control"
            aria-label="Text input with dropdown button"
            onChange={(e) => this.handleSearch(e)}
            placeholder="Tìm kiếm"
          />
        </div> */}

        <Input.Group compact>
          <Select
            name="key"
            defaultValue="id"
            style={{ width: "30%" }}
            onChange={(e) => this.props.handleChangeKeySearch(e, "key")}
          >
            <Option value={"id"}>Mã SV</Option>
            <Option value={"name"}>Tên</Option>
            <Option value={"phone"}>Số điện thoại</Option>
            <Option value={"email"}>Email</Option>
          </Select>
          <Input
            style={{ width: "70%" }}
            placeholder="Search"
            name="keyword"
            onChange={(e) =>
              this.props.handleChangeKeySearch(e.target.value, "keyword")
            }
          />
        </Input.Group>

        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">Mã SV</th>
              <th scope="col">Họ tên</th>
              <th scope="col">Số điện thoại</th>
              <th scope="col">Email</th>
              <th scope="col">Edit</th>
            </tr>
          </thead>
          <tbody>
            {this.props.dataTable
              .filter((e) =>
                e[this.props.search.key].includes(this.props.search.keyword)
              )
              .map((e, i) => {
                console.log(
                  e[this.props.search.key].includes(this.props.search.keyword)
                );
                // if (e[this.props.search.key].includes(this.props.search.keyword))
                return (
                  <tr>
                    <th scope="row">{e.id}</th>
                    <td>{e.name}</td>
                    <td>{e.phone}</td>
                    <td>{e.email}</td>
                    <td>
                      <button
                        className="btn btn-warning mx-1"
                        onClick={() => {
                          this.props.handleEditStudent(e);
                        }}
                      >
                        <i className="bi bi-pencil"></i>
                      </button>
                      <button
                        className="btn btn-danger mx-1"
                        onClick={() => {
                          this.props.handleDeleteStudent(e.id);
                        }}
                      >
                        <i className="bi bi-trash3"></i>
                      </button>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dataTable: state.userReducer.data,
    search: state.userReducer.search,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleEditStudent: (student) => {
      dispatch({
        type: EDIT_STUDENT,
        payload: student,
      });
    },
    handleDeleteStudent: (id) => {
      dispatch({
        type: DELETE_STUDENT,
        payload: id,
      });
    },
    handleChangeKeySearch: (value, target) => {
      dispatch({
        type: ON_CHANGE_KEY_SEARCH,
        payload: { key: value, target: target },
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TableManage);
