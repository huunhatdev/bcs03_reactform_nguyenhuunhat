import {
  ADD_STUDENT,
  DELETE_STUDENT,
  EDIT_STUDENT,
  ON_CHANGE,
  ON_CHANGE_KEY_SEARCH,
  ON_VALIDATE,
  UPDATE_STUDENT,
} from "../constant/constant";

const initialState = {
  info: {
    id: "",
    name: "",
    phone: "",
    email: "",
  },
  validate: {
    id: "",
    name: "",
    phone: "",
    email: "",
  },
  editUser: false,
  data: [],
  modal: {
    isVisible: true,
    text: "Đã tồn tại người dùng",
  },
  search: {
    key: "id",
    keyword: "",
  },
};

export default (state = initialState, { type, payload }) => {
  let dataClone = [...state.data];

  switch (type) {
    case ON_CHANGE:
      return {
        ...state,
        info: { ...state.info, [payload.name]: payload.value },
      };

    case ADD_STUDENT:
      var i = dataClone.findIndex((e) => {
        return e.id === payload.id;
      });
      if (i === -1) {
        dataClone.push(payload);
      } else {
        return {
          ...state,
          modal: {
            isVisible: true,
            text: "Đã tồn tại người dùng",
          },
          validate: {
            id: "Mã sinh viên đã tồn tại",
            name: "",
            phone: "",
            email: "",
          },
        };
      }
      var json = JSON.stringify({
        ...state,
        data: dataClone,
        info: {
          id: "",
          name: "",
          phone: "",
          email: "",
        },
        editUser: false,
        validate: {
          id: "",
          name: "",
          phone: "",
          email: "",
        },
      });
      return { ...JSON.parse(json) };

    case UPDATE_STUDENT:
      dataClone = [...state.data];
      i = dataClone.findIndex((e) => {
        return e.id === payload.id;
      });
      if (i !== -1) {
        dataClone[i] = payload;
        json = JSON.stringify({
          ...state,
          data: dataClone,
          validate: {
            id: "",
            name: "",
            phone: "",
            email: "",
          },
          info: {
            id: "",
            name: "",
            phone: "",
            email: "",
          },
          editUser: false,
        });
        return { ...JSON.parse(json) };
      }
      break;

    case EDIT_STUDENT:
      dataClone = [...state.data];
      i = dataClone.findIndex((e) => {
        return e.id === payload.id;
      });
      if (i !== -1) {
        json = JSON.stringify({ ...state, info: payload, editUser: true });
        return { ...JSON.parse(json) };
      }
      break;

    case DELETE_STUDENT:
      dataClone = [...state.data];
      i = dataClone.findIndex((e) => {
        return e.id === payload;
      });
      if (i !== -1) {
        dataClone.splice(i, 1);
        json = JSON.stringify({ ...state, data: dataClone });
        return { ...JSON.parse(json) };
      }
      break;

    case ON_VALIDATE:
      json = JSON.stringify({ ...state, validate: payload });
      return { ...JSON.parse(json) };

    case ON_CHANGE_KEY_SEARCH:
      json = JSON.stringify({
        ...state,
        search: { ...state.search, [payload.target]: payload.key },
      });

      return { ...JSON.parse(json) };

    default:
      return state;
  }
};
