import React, { Component } from "react";
import Infomation from "./Infomation";
import TableManage from "./TableManage";

export default class Form extends Component {
  render() {
    return (
      <div className="container">
        <Infomation />
        <TableManage />
      </div>
    );
  }
}
