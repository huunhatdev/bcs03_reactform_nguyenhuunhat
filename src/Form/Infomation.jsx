import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Form, Input } from "antd";
import { validate } from "./Validate";
import {
  ADD_STUDENT,
  ON_CHANGE,
  ON_VALIDATE,
  UPDATE_STUDENT,
} from "./redux/constant/constant";

class Infomation extends React.Component {
  handleGetValue = (e) => {
    let { name, value } = e.target;
    this.props.setValueInfo({ name, value });
  };

  render() {
    const onFinish = (values) => {
      const { valid, onFalse } = validate(values);
      if (!onFalse) {
        this.props.editUser
          ? this.props.handleEditStudent(values)
          : this.props.handleAddStudent(values);
      } else {
        this.props.handleValidate(valid);
      }
    };

    const onFinishFailed = (errorInfo) => {};

    return (
      <div>
        <h2 className="bg-dark text-white p-2">Thông tin sinh viên</h2>
        <div>
          <div className="row">
            <div className="col-6">
              <label>Mã sinh viên</label>
              <small style={{ color: "red", marginLeft: "5px" }}>
                {this.props.valid.id}
              </small>
              <Input
                disabled={this.props.editUser}
                name="id"
                onChange={(e) => this.handleGetValue(e)}
                value={this.props.info.id}
                status={this.props.valid.id ? "error" : ""}
              />
            </div>
            <div className="col-6">
              <label>Họ và tên</label>{" "}
              <small style={{ color: "red", marginLeft: "5px" }}>
                {this.props.valid.name}
              </small>
              <Input
                name="name"
                onChange={(e) => this.handleGetValue(e)}
                value={this.props.info.name}
                status={this.props.valid.name ? "error" : ""}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-6">
              <label>Số điện thoại</label>{" "}
              <small style={{ color: "red", marginLeft: "5px" }}>
                {this.props.valid.phone}
              </small>
              <Input
                name="phone"
                onChange={(e) => this.handleGetValue(e)}
                value={this.props.info.phone}
                status={this.props.valid.phone ? "error" : ""}
              />
            </div>
            <div className="col-6">
              <label>Email</label>{" "}
              <small style={{ color: "red", marginLeft: "5px" }}>
                {this.props.valid.email}
              </small>
              <Input
                name="email"
                onChange={(e) => this.handleGetValue(e)}
                value={this.props.info.email}
                status={this.props.valid.email ? "error" : ""}
              />
            </div>
          </div>
          <Button
            type="primary"
            className="mb-3"
            htmlType="submit"
            onClick={() => onFinish(this.props.info)}
          >
            {this.props.editUser ? "Cập nhật" : "Thêm sinh viên"}
          </Button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    info: state.userReducer.info,
    editUser: state.userReducer.editUser,
    valid: state.userReducer.validate,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAddStudent: (student) => {
      dispatch({
        type: ADD_STUDENT,
        payload: student,
      });
    },
    handleEditStudent: (student) => {
      dispatch({
        type: UPDATE_STUDENT,
        payload: student,
      });
    },
    setValueInfo: (value) => {
      dispatch({
        type: ON_CHANGE,
        payload: value,
      });
    },
    handleValidate: (valid) => {
      dispatch({
        type: ON_VALIDATE,
        payload: valid,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Infomation);
